<?php

namespace Gupo\ApolloConfig\Support;

/**
 * 快速构建类的实例
 */
trait Singleton
{
    /**
     * @var static 单例对象
     */
    private static $instance;

    /**
     * 单例
     * @param ...$parameters
     * @return static
     */
    public static function instance(...$parameters): self
    {
        if (!isset(self::$instance)) {
            self::$instance = new static(...$parameters);
        }
        return self::$instance;
    }
}
