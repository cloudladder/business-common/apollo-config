<?php

namespace Gupo\ApolloConfig\Support;

interface ConfigInterface
{
    public function setConfigName(string $configName): ConfigInterface;

    public function get();
}