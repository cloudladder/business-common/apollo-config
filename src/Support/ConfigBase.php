<?php

namespace Gupo\ApolloConfig\Support;

abstract class ConfigBase implements ConfigInterface
{
    /**
     * 配置文件名称
     * @var string
     */
    protected string $configName;


    /**
     * @param  string  $configName
     * @return $this
     */
    public function setConfigName(string $configName): ConfigBase
    {
        $this->configName = $configName;
        return $this;
    }
}