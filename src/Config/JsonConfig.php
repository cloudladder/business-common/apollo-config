<?php

namespace Gupo\ApolloConfig\Config;

use Gupo\ApolloConfig\Exception\ConfigNotFoundException;
use Gupo\ApolloConfig\Support\ConfigBase;
use Gupo\ApolloConfig\Support\ConfigInterface;

class JsonConfig extends ConfigBase implements ConfigInterface
{
    /**
     * 配置项名称
     * @var string
     */
    protected string $key;


    /**
     * @param  string  $key
     * @return $this
     */
    public function setKey(string $key): JsonConfig
    {
        $this->key = $key;
        return $this;
    }


    /**
     * @throws ConfigNotFoundException
     */
    public function get()
    {
        $configPath = get_config_path($this->configName);
        if (!config_exists($configPath)) {
            return null;
        }

        $config = json_decode(file_get_contents($configPath), true);
        if (empty($this->key)) {
            return $config;
        }

        foreach (explode('.', $this->key) as $segment) {
            if (array_key_exists($segment, $config)) {
                $config = $config[$segment];
            }
        }

        return $config;
    }
}