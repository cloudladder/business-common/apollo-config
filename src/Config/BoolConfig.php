<?php

namespace Gupo\ApolloConfig\Config;

use Gupo\ApolloConfig\Exception\ConfigNotFoundException;
use Gupo\ApolloConfig\Support\ConfigBase;
use Gupo\ApolloConfig\Support\ConfigInterface;

class BoolConfig extends ConfigBase implements ConfigInterface
{
    /**
     * @return bool|null
     * @throws ConfigNotFoundException
     */
    public function get(): ?bool
    {
        $configPath = get_config_path($this->configName);
        if (!config_exists($configPath)) {
            return null;
        }

        return (bool) file_get_contents($configPath);
    }
}