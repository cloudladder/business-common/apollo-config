<?php

namespace Gupo\ApolloConfig\Config;

use Gupo\ApolloConfig\Exception\ConfigNotFoundException;
use Gupo\ApolloConfig\Support\ConfigBase;
use Gupo\ApolloConfig\Support\ConfigInterface;

class IntConfig extends ConfigBase implements ConfigInterface
{
    /**
     * @return int|null
     * @throws ConfigNotFoundException
     */
    public function get(): ?int
    {
        $configPath = get_config_path($this->configName);
        if (!config_exists($configPath)) {
            return null;
        }

        $value = file_get_contents($configPath);
        if (!is_numeric($value)) {
            return null;
        }

        return (int) $value;
    }
}