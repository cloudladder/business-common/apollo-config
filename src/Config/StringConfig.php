<?php

namespace Gupo\ApolloConfig\Config;

use Gupo\ApolloConfig\Exception\ConfigNotFoundException;
use Gupo\ApolloConfig\Support\ConfigBase;
use Gupo\ApolloConfig\Support\ConfigInterface;

class StringConfig extends ConfigBase implements ConfigInterface
{
    /**
     * @return string|null
     * @throws ConfigNotFoundException
     */
    public function get(): ?string
    {
        $configPath = get_config_path($this->configName);
        if (!config_exists($configPath)) {
            return null;
        }

        return (string) file_get_contents($configPath);
    }
}