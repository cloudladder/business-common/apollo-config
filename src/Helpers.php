<?php


use Gupo\ApolloConfig\ApolloConfig;
use Gupo\ApolloConfig\Exception\ConfigNotFoundException;

if (!function_exists('get_config_path')) {
    /**
     * 判断配置文件是否存在
     * @param  string  $configName
     * @return string
     * @throws ConfigNotFoundException
     */
    function get_config_path(string $configName): string
    {
        $confBaseDir = ApolloConfig::instance()->getConfBaseDir();
        if (empty($confBaseDir)) {
            throw new ConfigNotFoundException('未设置配置文件根目录');
        }

        if (empty($configName)) {
            throw new ConfigNotFoundException('配置名称不能为空');
        }

        return $confBaseDir.'/'.$configName;
    }
}

if (!function_exists('config_exists')) {
    /**
     * 判断配置文件是否存在
     * @param  string  $configPath
     * @return bool
     */
    function config_exists(string $configPath): bool
    {
        return file_exists($configPath);
    }
}
