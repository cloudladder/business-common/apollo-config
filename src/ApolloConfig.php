<?php

namespace Gupo\ApolloConfig;

use Gupo\ApolloConfig\Config\BoolConfig;
use Gupo\ApolloConfig\Config\FloatConfig;
use Gupo\ApolloConfig\Config\IntConfig;
use Gupo\ApolloConfig\Config\JsonConfig;
use Gupo\ApolloConfig\Config\StringConfig;
use Gupo\ApolloConfig\Exception\ConfigNotFoundException;
use Gupo\ApolloConfig\Support\Singleton;

class ApolloConfig
{
    use Singleton;

    /**
     * 配置文件基础目录
     * @var string
     */
    private string $confBaseDir;

    /**
     * @param  string  $confBaseDir
     */
    private function __construct(string $confBaseDir = '')
    {
        if (empty($confBaseDir)) {
            $confBaseDir = (string) getenv('APOLLO_CONF_BASE_DIR'); // 项目自定义的配置文件目录，适用于本地开发
        }
        if (empty($confBaseDir)) {
            $confBaseDir = getenv('CONF_FILE_PATH') ?: '/var/www/conf'; // K8S中的配置文件目录
        }

        $this->confBaseDir = $confBaseDir;
    }

    /**
     * @return string
     */
    public function getConfBaseDir(): string
    {
        return $this->confBaseDir;
    }

    /**
     * @return JsonConfig
     */
    public function json(): JsonConfig
    {
        return new JsonConfig();
    }

    /**
     * @return StringConfig
     */
    public function string(): StringConfig
    {
        return new StringConfig();
    }

    /**
     * @return BoolConfig
     */
    public function bool(): BoolConfig
    {
        return new BoolConfig();
    }

    /**
     * @return FloatConfig
     */
    public function float(): FloatConfig
    {
        return new FloatConfig();
    }

    /**
     * @return IntConfig
     */
    public function int(): IntConfig
    {
        return new IntConfig();
    }
}