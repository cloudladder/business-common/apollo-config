# Apollo-Config -- Apollo配置中心工具包

Apollo-Config是用于搭配Apollo配置中心使用的PHP工具包，主要提供配置项的解析功能，在开发时可以优雅便捷的获取配置项。已支持当前Apollo中的所有配置类型（Json、String、Number、Boolean），且方便扩展。

## 快速开始

### 安装

```shell
composer require gupo/apollo-config
```

### 使用

```php
// 配置文件基础目录
const CONF_BASE_PATH = __DIR__.'/conf';

// 实例化ApolloConfig对象，并设置配置文件基础目录。 
$config = ApolloConfig::instance(CONF_BASE_PATH); // 配置文件基础目录可以不传，不传时从环境变量`APOLLO_CONF_BASE_DIR`中读取

// 获取json类型的配置文件
$res = $config->json()
    ->setConfigName('test_json')
    ->setKey('h.e.l.l.o.3')
    ->get();
    
var_dump($res); // 输出: string(1) "r"
```

- 更多使用示例，参考 [example/index.php](./example/index.php)

### Apollo配置中心操作指南

[点这里查看](https://alidocs.dingtalk.com/i/nodes/ZgpG2NdyVXrvGoq6HD1q9AnX8MwvDqPk?utm_scene=team_space)
以.env文件为例，可根据需求配置

## 为什么使用配置中心

我们在开发时经常需要设置一些持久化的配置项，例如白名单、映射map等，这些配置经常需要修改，所以不能写在代码中，在之前只能选择`.env文件`
或在`数据库`中配置，但`.env`文件没有层级，只能支持简单类型的字符串，另外`.env`文件更适合用于设置系统级的配置（例如数据地址、密码），配置中心中的配置更偏向于业务级别的配置，
而数据库的资源昂贵，新建一张表专门用来存储配置不仅有些浪费存储资源，还有可能影响到其他业务的查询效率，另外新增/修改配置可能会需要DBA审批，也会影响效率。
Apollo完美的解决了以上问题，Apollo支持多种类型的配置（Json、String、Number、Boolean）、支持多环境、鉴权，配置项发布后可实时同步到K8S容器内，了解更多可查看[Apollo配置中心文档](https://www.apolloconfig.com/#/zh/design/apollo-introduction)。



