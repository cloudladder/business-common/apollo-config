<?php

require __DIR__.'/../vendor/autoload.php';

use Gupo\ApolloConfig\ApolloConfig;

const CONF_BASE_PATH = __DIR__.'/conf';

// 实例化ApolloConfig对象，并设置配置文件基础目录。 配置文件基础目录可以不传，不传时从环境变量`APOLLO_CONF_BASE_DIR`中读取
$config = ApolloConfig::instance(CONF_BASE_PATH);

// 获取json类型的配置文件
$res = $config->json()
    ->setConfigName('test_json')
    ->setKey('h.e.l.l.o.3')
    ->get();
var_dump($res);

// 获取string类型的配置文件
$res = $config->string()->setConfigName('test_str')->get();
var_dump($res);

// 获取bool类型的配置文件
$res = $config->bool()->setConfigName('test_bool')->get();
var_dump($res);

// 获取float类型的配置文件
$res = $config->float()->setConfigName('test_number')->get();
var_dump($res);

// 获取int类型的配置文件
$res = $config->int()->setConfigName('test_number')->get();
var_dump($res);


